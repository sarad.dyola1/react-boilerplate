import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "./Routes";
import "./Assets/sass/main.scss";

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem("token") ? (
          <Component {...props} />
        ) : (
          <Redirect to="/login" />
        )
      }
    ></Route>
  );
};
function App() {
  return (
    <Switch>
      {routes.map((route, i) => {
        return route.private ? (
          <PrivateRoute key={i} {...route} />
        ) : (
          <Route key={i} {...route} />
        );
      })}
    </Switch>
  );
}

export default App;
