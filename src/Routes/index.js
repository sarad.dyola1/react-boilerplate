import Home from "./../Views/Pages/Home";
import PageNotFound from "./../Views/Pages/PageNotFound";

export default [
  {
    path: "/",
    component: Home,
    exact: true
  },
  {
    component: PageNotFound
  }
];
